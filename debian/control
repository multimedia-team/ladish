Source: ladish
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python2,
 dh-python,
 intltool,
 libasound2-dev,
 libboost-dev,
 libdbus-1-dev,
 libdbus-glib-1-dev,
 libexpat1-dev,
 libflowcanvas-dev (>> 0.6.4),
 libglade2-dev,
 libgnomecanvasmm-2.6-dev,
 libgtk2.0-dev,
 libjack-jackd2-dev (>= 1.9.6~dfsg.1-5),
 python2 (>= 2.6.6-3),
 uuid-dev
Standards-Version: 4.5.1
Homepage: https://ladish.org/
Vcs-Git: https://salsa.debian.org/multimedia-team/ladish.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/ladish

Package: ladish
Architecture: any
Depends:
 jackd,
 python-dbus,
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Suggests:
 gladish
Recommends:
 a2jmidid
Description: session management system for JACK applications
 LADI Session Handler or simply ladish is a session management
 system for JACK applications on GNU/Linux. Its aim is to allow
 you to have many different audio programs running at once, to
 save their setup, close them down and then easily reload the
 setup at some other time. ladish doesn't deal with any kind of
 audio or MIDI data itself; it just runs programs, deals with
 saving/loading (arbitrary) data and connects JACK ports
 together. It can also be used to move entire sessions between
 computers, or post sessions on the Internet for download.

Package: gladish
Architecture: any
Depends:
 ladish (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 laditools
Description: graphical interface for LADI Session Handler
 LADI Session Handler or simply ladish is a session management
 system for JACK applications on GNU/Linux. Its aim is to allow
 you to have many different audio programs running at once, to
 save their setup, close them down and then easily reload the
 setup at some other time.
 .
 This package provides a graphical interface to ladish.

Package: liblash-compat-1debian0
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: LASH compatibility library
 liblash-compat is a library and associated header files, that aims
 to provide lash environment for apps that can use it. Currently it
 does not interact with ladish daemon yet.
 .
 This package provides the shared library.

Package: liblash-compat-dev
Section: libdevel
Architecture: all
Depends:
 libdbus-1-dev,
 liblash-compat-1debian0 (<< ${source:Upstream-Version}+1~),
 liblash-compat-1debian0 (>= ${source:Version}),
 uuid-dev,
 ${misc:Depends}
Description: LASH compatibility library (development files)
 liblash-compat is a library and associated header files, that aims
 to provide lash environment for apps that can use it. Currently it
 does not interact with ladish daemon yet. In its current state, it
 enables building lash-aware apps but they will fail to contact the
 lash server.
 .
 In its current state, it enables building lash-aware apps but they
 will fail to contact the lash server.
 .
 This package provides the development files.
